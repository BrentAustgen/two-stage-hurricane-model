#!/bin/bash
echo "01"
jupyter execute pre-01-remap.ipynb
echo "02"
jupyter execute pre-02-specs-grid.ipynb
echo "03"
jupyter execute pre-03-specs-grid-reduced.ipynb
echo "04"
jupyter execute pre-04-clean-flood-data.ipynb
echo "05"
jupyter execute pre-05-specs-flooding.ipynb
echo "06"
jupyter execute pre-06-model-options.ipynb
echo "07"
jupyter execute pre-07-big-M.ipynb
echo "08"
jupyter execute pre-08-merge.ipynb
