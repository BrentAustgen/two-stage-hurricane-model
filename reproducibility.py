import json
import time
from collections import defaultdict

import matplotlib.pyplot as plt
import networkx as nx


def build_dependency_graph(target, dependencies):

    filenames = set()
    filenames.add(target)
    check = filenames.copy()
    while True:
        neighbors = set()
        for node in check:
            neighbors |= set(dependencies[node][0])
        check = neighbors - filenames
        if len(check) == 0:
            break
        else:
            filenames |= neighbors

    edges = set()
    nodes = dict()
    for ofilename in filenames:
        ifilenames, script = dependencies[ofilename]
        if script is not None:
            nodes[ofilename] = '#0000ff'
            nodes[script] = '#ffff00'
            edges.add((script, ofilename))
            for ifilename in ifilenames:
                edges.add((ifilename, script))
        else:
            nodes[ofilename] = '#00ff00'
    nodes[target] = '#ff0000'
    graph = nx.DiGraph()
    graph.add_edges_from(edges)
    return graph


with open('reproducibility.json') as fh:
    data = json.load(fh)

dependencies = defaultdict(lambda: ([], None))
for script, processes in data.items():
    for process in processes:
        for o in process['output']:
            dependencies[o] = (process['input'], script)


targets = {
    "Figure 1": ["results/figures/iise-nomenclature.jpg"],
    "Figure 2": ["results/figures/iise-model-diagram.jpg"],
    "Figure 3": [
        "results/figures/iise-original.jpg",
        "results/figures/iise-reduced.jpg"
    ],
    "Figure 4": ["results/figures/iise-mitigation-levels.eps"],
    "Figure 5": ["results/figures/iise-[casestudy]-uncertainty-heatmap.jpg"],
    "Figure 6": ["results/figures/iise-harvey-landfall-distribution.eps"],
    "Figure 7": ["results/figures/iise-flooding-[casestudy].jpg"],
    "Figure 8": ["results/figures/iise-[casestudy]-uncertainty-heatmap.jpg"],
    "Figure 9": ["results/figures/iise-solution-times.eps"],
    "Figure 10": ["results/figures/iise-obj-bnds.eps"],
    "Figure 11": ["results/figures/iise-[casestudy]-stochastic-solutions.jpg"],
    "Figure 12": ["results/figures/iise-improvement.jpg"],
    "Figure 13": ["results/figures/iise-mitigation-[casestudy]-[powerflow]-stochastic-r[r_hat]-f[f].jpg"],
    "Table 4": [
        "results/ws/[casestudy]-[powerflow]-f[f]-omega[omega]-r[r_hat].zip",
        "results/ev/[casestudy]-[powerflow]-f[f]-r[r_hat].zip",
        "results/eev/[casestudy]-[powerflow]-f[f]-r[r_hat].zip",
        "results/sp/[casestudy]-[powerflow]-f[f]-r[r_hat].zip"
    ],
    "Table 5": [
        "results/ws/[casestudy]-[powerflow]-f[f]-omega[omega]-r[r_hat].zip",
        "results/ev/[casestudy]-[powerflow]-f[f]-r[r_hat].zip",
        "results/eev/[casestudy]-[powerflow]-f[f]-r[r_hat].zip",
        "results/sp/[casestudy]-[powerflow]-f[f]-r[r_hat].zip"
    ]
}


print()
for figure, filenames in targets.items():
    print(figure)
    for filename in filenames:
        graph = build_dependency_graph(filename, dependencies)
        print('  ' + filename)
        scripts_pre = set(node for node in graph.nodes if node.startswith('scripts/pre'))
        for script in sorted(scripts_pre):
            print('    ' + script)
        scripts_post = set(node for node in graph.nodes if node.startswith('scripts/post'))
        for script in sorted(scripts_post):
            print('    ' + script)
        scripts_opt = set(node for node in graph.nodes if node.startswith('scripts/opt'))
        for script in sorted(scripts_opt):
            print('    ' + script)
        scripts_iise = set(node for node in graph.nodes if node.startswith('scripts/iise'))
        for script in sorted(scripts_iise):
            print('    ' + script)
        inputs = set(node for node in graph.nodes if node.startswith('data/original'))
        for filename in sorted(inputs):
            print('    ' + filename)
    print()

