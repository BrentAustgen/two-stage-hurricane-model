#!/bin/bash

rhat="4"
casestudy="harvey-tigerdam-disc"
pftype="lpdc"
approach="robust"

for f in $(seq 121 160)
do
    sbatch warmstart-$rhat-$f-$casestudy-$pftype-$approach.batch
done
