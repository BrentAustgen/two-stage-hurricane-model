#!/bin/bash

rhat="4"
casestudy="harvey-tigerdam-disc"
pftype="lpdc"
approach="robust"
sbatch_timelimit="00:15:00"
solver_timelimit="$(((15 - 5) * 60))"


for f in $(seq 0 304)
do
    cp template.batch warmstart-$rhat-$f-$casestudy-$pftype-$approach.batch
    sed -i\
        -e "s/<rhat>/$rhat/"\
        -e "s/<f>/$f/"\
        -e "s/<pftype>/$pftype/"\
        -e "s/<casestudy>/$casestudy/"\
        -e "s/<approach>/$approach/"\
        -e "s/<sbatch_timelimit>/${sbatch_timelimit}/"\
        -e "s/<solver_timelimit>/${solver_timelimit}/"\
        warmstart-$rhat-$f-$casestudy-$pftype-$approach.batch
done
