#!/bin/sh

rhat="4"
casestudy="harvey-tigerdam-disc"
pftype="lpdc"
approach="robust"

for f in $(seq 40 80)
do
    echo -n "$f "
    filename=$(ls warmstart-$rhat-$f-${pftype}-${casestudy}-${approach}-*.out 2>/dev/null)
    if [[ "$filename" == "" ]]
    then
        echo "pending"
    else
        result=$(grep -o "gap.*$" $filename)
        if [[ "$result" == "" ]]
        then
            echo "running"
        else
            echo $result
        fi
    fi
done
