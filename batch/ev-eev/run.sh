#!/bin/bash

casestudy="harvey-tigerdam-disc"
pftype="lpac-loose"
approach="stochastic"

for f in $(seq 51 66)
do
    sbatch ev-eev-$f-$casestudy-$pftype-$approach.batch
done
