#!/bin/sh

rhat="3"
casestudy="harvey-tigerdam-disc"
pftype="lpdc"
approach="stochastic"

for f in $(seq 0 193)
do
    echo -n "$f "
    filename=$(ls nogood-$rhat-$f-${pftype}-${casestudy}-${approach}-*.out 2>/dev/null)
    if [[ "$filename" == "" ]]
    then
        echo "pending"
    else
        result=$(grep -o "gap.*$" $filename)
        if [[ "$result" == "" ]]
        then
            echo "running"
        else
            echo $result
        fi
    fi
done
