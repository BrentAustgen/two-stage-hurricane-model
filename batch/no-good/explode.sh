#!/bin/bash

rhat="3"
casestudy="imelda-tigerdam-disc"
pftype="lpdc"
approach="stochastic"
sbatch_timelimit="00:30:00"
solver_timelimit="$(((30 - 5) * 60))"


for f in $(seq 1 1)
do
    cp template.batch nogood-$rhat-$f-$casestudy-$pftype-$approach.batch
    sed -i\
        -e "s/<rhat>/$rhat/"\
        -e "s/<f>/$f/"\
        -e "s/<pftype>/$pftype/"\
        -e "s/<casestudy>/$casestudy/"\
        -e "s/<approach>/$approach/"\
        -e "s/<sbatch_timelimit>/${sbatch_timelimit}/"\
        -e "s/<solver_timelimit>/${solver_timelimit}/"\
        nogood-$rhat-$f-$casestudy-$pftype-$approach.batch
done
