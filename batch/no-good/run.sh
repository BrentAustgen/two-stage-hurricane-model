#!/bin/bash

rhat="3"
casestudy="imelda-tigerdam-disc"
pftype="lpdc"
approach="stochastic"

for f in $(seq 1 1)
do
    sbatch nogood-$rhat-$f-$casestudy-$pftype-$approach.batch
done
