#!/bin/sh

casestudy="harvey-tigerdam-disc"
pftype="lpac-loose"
approach="robust"

for f in $(seq 41 66)
do
    echo -n "$f "
    filename=$(ls mv-mmv-$f-${pftype}-${casestudy}-${approach}-*.out 2>/dev/null)
    if [[ "$filename" == "" ]]
    then
        echo "pending"
    else
        result=$(grep -o "gap.*$" $filename)
        if [[ "$result" == "" ]]
        then
            echo "running"
        else
            echo $result
        fi
    fi
done
