#!/bin/bash

casestudy="harvey-tigerdam-disc"
pftype="lpac-loose"
approach="robust"
sbatch_timelimit="01:00:00"
solver_timelimit="$(((60 - 5) * 60))"


for f in $(seq 0 66)
do
    cp template.batch mv-mmv-$f-$casestudy-$pftype-$approach.batch
    sed -i\
        -e "s/<f>/$f/"\
        -e "s/<pftype>/$pftype/"\
        -e "s/<casestudy>/$casestudy/"\
        -e "s/<approach>/$approach/"\
        -e "s/<sbatch_timelimit>/${sbatch_timelimit}/"\
        -e "s/<solver_timelimit>/${solver_timelimit}/"\
        mv-mmv-$f-$casestudy-$pftype-$approach.batch
done
