#!/bin/bash

casestudy="harvey-tigerdam-disc"
pftype="lpac-loose"
approach="robust"

for f in $(seq 63 66)
do
    sbatch mv-mmv-$f-$casestudy-$pftype-$approach.batch
done
