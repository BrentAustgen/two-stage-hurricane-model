#!/bin/bash

rhat="3"
casestudy="harvey-tigerdam-disc"
pftype="lpac-loose"
approach="robust"
sbatch_timelimit="02:00:00"
fmin=0
fmax=62


cp template.batch acopf-$rhat-$fmin-$fmax-$casestudy-$pftype-$approach.batch
sed -i\
    -e "s/<rhat>/$rhat/"\
    -e "s/<fmin>/$fmin/"\
    -e "s/<fmax>/$fmax/"\
    -e "s/<pftype>/$pftype/"\
    -e "s/<casestudy>/$casestudy/"\
    -e "s/<approach>/$approach/"\
    -e "s/<sbatch_timelimit>/${sbatch_timelimit}/"\
    acopf-$rhat-$fmin-$fmax-$casestudy-$pftype-$approach.batch
